module gitlab.com/SeattleSocial/spaces-cdn-operator

require (
	github.com/digitalocean/godo v1.17.0
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
)
