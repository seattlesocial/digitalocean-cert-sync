package main

import (
	"context"
	"crypto/sha1"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/digitalocean/godo"
	"golang.org/x/oauth2"
)

var (
	certFile  = "tls.crt"
	keyFile   = "tls.key"
	endpoint  = os.Getenv("CDN_ENDPOINT")
	cdnDomain = os.Getenv("CDN_DOMAIN")
	doClient  *godo.Client
)

type doToken struct {
	AccessToken string
}

func (t *doToken) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{
		AccessToken: os.Getenv("DIGITALOCEAN_TOKEN"),
	}
	return token, nil
}

func main() {
	if endpoint == "" {
		log.Println("CDN_ENDPOINT not defined!")
		os.Exit(1)
	}

	if cdnDomain == "" {
		log.Println("CDN_DOMAIN not defined!")
		os.Exit(1)
	}

	if dir := os.Getenv("CERTIFICATE_DIRECTORY"); dir != "" {
		certFile = fmt.Sprintf("%s/%s", dir, certFile)
		keyFile = fmt.Sprintf("%s/%s", dir, keyFile)
	}

	oauthClient := oauth2.NewClient(oauth2.NoContext, &doToken{})
	doClient = godo.NewClient(oauthClient)

	log.Printf("Reading key from %s", keyFile)
	key, err := ioutil.ReadFile(keyFile)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Reading cert from %s", certFile)
	cert, err := ioutil.ReadFile(certFile)
	if err != nil {
		log.Fatal(err)
	}

	pemBlock, _ := pem.Decode(cert)
	parsedCert, err := x509.ParseCertificate(pemBlock.Bytes)
	if err != nil {
		log.Fatal(err)
	}

	certSha := fmt.Sprintf("%x", sha1.Sum(parsedCert.Raw))

	certID, err := ensureCertUploaded(certSha, parsedCert, cert, key)
	if err != nil {
		log.Fatal(err)
	}

	err = ensureEndpoint(certID)
	if err != nil {
		log.Fatal(err)
	}
}

func ensureCertUploaded(certSha string, parsedCert *x509.Certificate, cert []byte, key []byte) (string, error) {
	opt := &godo.ListOptions{
		Page:    1,
		PerPage: 200,
	}

	log.Println("Checking if DigitalOcean already knows about our certificate...")
	certs, _, err := doClient.Certificates.List(context.TODO(), opt)
	if err != nil {
		log.Fatal(err)
	}

	for _, c := range certs {
		if c.SHA1Fingerprint == certSha {
			log.Printf("DigitalOcean already knows about cert with sha %s", certSha)
			return c.ID, nil
		}
		notAfter, err := time.Parse(time.RFC3339, c.NotAfter)
		if err != nil {
			log.Printf("Failed to parse cert %s's NotAfter of %s: %s", c.Name, c.NotAfter, err.Error())
			continue
		}
		if notAfter.After(time.Now()) {
			log.Printf("Cert %s expired on %s. Deleting", c.Name, c.NotAfter)
			doClient.Certificates.Delete(context.Background(), c.ID)
		}
	}

	log.Printf("Uploading new certificate with sha %s for domain %s", certSha, parsedCert.DNSNames[0])

	createRequest := &godo.CertificateRequest{
		Name:             fmt.Sprintf("%s-%s", parsedCert.DNSNames[0], time.Now().Format("2006-Jan-2")),
		PrivateKey:       string(key),
		LeafCertificate:  string(pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: parsedCert.Raw})),
		CertificateChain: "",
		Type:             "custom",
	}

	uploadedCert, _, err := doClient.Certificates.Create(context.TODO(), createRequest)
	if err != nil {
		log.Fatal(err)
	}
	return uploadedCert.ID, nil
}

func ensureEndpoint(certID string) error {
	opt := &godo.ListOptions{
		Page:    1,
		PerPage: 200,
	}

	log.Println("Checking if the CDN endpoint already exists")
	var cdnID string
	var cdnCertID string
	cdns, _, err := doClient.CDNs.List(context.TODO(), opt)
	if err != nil {
		return err
	}

	for _, cdn := range cdns {
		if cdn.CustomDomain == cdnDomain {
			log.Printf("Using CDN with endpoint %s", cdn.Endpoint)
			cdnID = cdn.ID
			cdnCertID = cdn.CertificateID
			break
		}
	}

	if cdnID == "" {
		createRequest := &godo.CDNCreateRequest{
			Origin:        endpoint,
			TTL:           604800,
			CustomDomain:  cdnDomain,
			CertificateID: certID,
		}

		log.Printf("CDN endpoint does not exist. Creating (%+v)", createRequest)
		cdn, _, err := doClient.CDNs.Create(context.TODO(), createRequest)
		if err != nil {
			return err
		}

		cdnID = cdn.ID
		cdnCertID = cdn.CertificateID
	}

	if cdnCertID != certID {
		log.Printf("CDN Endpoint is using a different certificate (%s). Updating to use current cert (%s)", cdnCertID, certID)
		updateRequest := &godo.CDNUpdateCustomDomainRequest{
			CertificateID: certID,
			CustomDomain:  cdnDomain,
		}
		_, _, err = doClient.CDNs.UpdateCustomDomain(context.TODO(), cdnID, updateRequest)
		if err != nil {
			return err
		}
	} else {
		log.Printf("CDN endpoint %s has correct certificate %s", cdnID, certID)
	}
	return nil

}
