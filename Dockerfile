FROM golang:latest AS build
RUN mkdir -p /go/src/gitlab.com/SeattleSocial/spaces-cdn-operator
WORKDIR /go/src/gitlab.com/SeattleSocial/spaces-cdn-operator
ENV GO111MODULE=on
COPY * /go/src/gitlab.com/SeattleSocial/spaces-cdn-operator/
RUN go mod download
RUN CGO_ENABLED=0 go build

FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /go/src/gitlab.com/SeattleSocial/spaces-cdn-operator/spaces-cdn-operator /spaces-cdn-operator
ENTRYPOINT ["/spaces-cdn-operator"]
